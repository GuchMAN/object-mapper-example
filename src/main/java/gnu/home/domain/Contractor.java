package gnu.home.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Contractor {
    private String firstName;
    private String secondName;
}
