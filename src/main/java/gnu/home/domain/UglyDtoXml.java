package gnu.home.domain;

import lombok.Getter;
import lombok.Setter;

import javax.xml.datatype.XMLGregorianCalendar;

@Getter
@Setter
public class UglyDtoXml {
    private boolean cd;
    private String id;
    private double cta;
    private double dta;
    private String text;
    private String ean;
    private XMLGregorianCalendar date;
    private String cnm1;
    private String cnm2;
    private String bank;
}
