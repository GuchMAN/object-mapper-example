package gnu.home.domain;

public enum TransactionSides {
    CREDIT, DEBIT
}
