package gnu.home.domain.converters;

import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.springframework.util.StringUtils;

@Mapper
public interface BankConverter {

    @Named("bank")
    default String bankConvert(String bank) {
        return StringUtils.replace(bank, "OOO", "").trim();
    }
}
