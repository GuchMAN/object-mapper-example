package gnu.home.domain.converters;

import gnu.home.domain.Transaction;
import gnu.home.domain.UglyDtoXml;
import org.mapstruct.*;

@Mapper(uses = {
        BankConverter.class
})
public interface TransactionConverter {
    @Mappings({
            @Mapping(source = "cnm1", target = "contractor.firstName"),
            @Mapping(source = "cnm2", target = "contractor.secondName"),
            @Mapping(target = "date", dateFormat = "dd.MM.yyyy"),
            @Mapping(target = "amount", expression = "java( uglyDtoXml.isCd() ? uglyDtoXml.getCta() : uglyDtoXml.getDta() )"),
            @Mapping(target = "accountNumber", source = "ean", qualifiedByName = "eanFormat"),
            @Mapping(target = "id", source = "id", qualifiedByName = "cus"),
            @Mapping(target = "bank", qualifiedByName = "bank")
    })
    Transaction transaction(UglyDtoXml uglyDtoXml, @Context String cus);

    @Named("eanFormat")
    default String eanFormat(String ean) {
        return ean.trim();
    }

    @Named("cus")
    default String cus(String id, @Context String cus) {
        return id + cus;
    }

}
