package gnu.home.domain;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Transaction {
    private double amount;
//    private TransactionSides side;
    private String text;
    private String accountNumber;
    private Contractor contractor;
    private String date;
    private String id;
    private String bank;
}
