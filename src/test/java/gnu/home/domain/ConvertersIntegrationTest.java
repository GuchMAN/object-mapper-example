package gnu.home.domain;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import gnu.home.domain.converters.TransactionConverter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;

@SpringBootTest(classes = Application.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class ConvertersIntegrationTest {

    @Autowired
    private TransactionConverter converter;

    @Test
    public void testConverters() throws Exception {

        UglyDtoXml uglyDtoXml = new UglyDtoXml();
        uglyDtoXml.setCnm1("Ivanov");
        uglyDtoXml.setCnm2("Ivan");
        uglyDtoXml.setCd(true);
        uglyDtoXml.setCta(1.0);
        uglyDtoXml.setDta(2.0);
        uglyDtoXml.setEan("   Some text   ");
        uglyDtoXml.setText("Some text");
        uglyDtoXml.setId("id");
        uglyDtoXml.setBank("OOO Bank");
        uglyDtoXml.setDate(new XMLGregorianCalendarImpl(new GregorianCalendar(2016, 9, 15)));

        Transaction transaction = converter.transaction(uglyDtoXml,"cus");

        assertEquals(uglyDtoXml.getCnm1(), transaction.getContractor().getFirstName());
        assertEquals(uglyDtoXml.getCnm2(), transaction.getContractor().getSecondName());
        assertEquals(uglyDtoXml.getText(), transaction.getText());
        assertEquals("15.10.2016", transaction.getDate());
        assertEquals("idcus", transaction.getId());
        assertEquals("Bank", transaction.getBank());
        assertEquals("Some text", transaction.getAccountNumber());
        assertEquals(1.0, transaction.getAmount(), 0.00001);

    }
}
